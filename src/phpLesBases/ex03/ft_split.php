<?php

function ft_split($string): array
{
    $mots = preg_split("/[\s,]+/", $string, -1, PREG_SPLIT_NO_EMPTY);
    sort($mots);

    return $mots;
}
