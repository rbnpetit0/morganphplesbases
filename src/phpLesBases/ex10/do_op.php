<?php

// Récupérer 3 paramètres (argv) pas +
// Le 1er et le 3e DOIVENT être des nombres
// Le second DOIT être une opération arithmétique

$errormsg = "Incorrect Parameters\n";
// On déclare le message d'erreur pour le réutiliser plus tard

$tableauparametre = array_slice($argv, 1);
// On récupère la liste des paramètres sans prendre le code
if (count($tableauparametre) != 3) {
    echo $errormsg;
    exit();
}
// Si nous avons plus de trois paramètres, le programme se termine

$tableauparametre[0] = trim($tableauparametre[0]);
$tableauparametre[1] = trim($tableauparametre[1]);
$tableauparametre[2] = trim($tableauparametre[2]);
// On enlève les espaces des trois paramètres converti dans le tableau

if (is_numeric($tableauparametre[0]) && is_numeric($tableauparametre[2])) {
    // Si les paramètres 1 & 3 sont des nombres alors :
    switch ($tableauparametre[1]) {
        // Si le paramètre 2 est '+' alors...
        case '+':
            echo $tableauparametre[0] + $tableauparametre[2];
            break;
        case '-':
            echo $tableauparametre[0] - $tableauparametre[2];
            break;
        case '*':
            echo $tableauparametre[0] * $tableauparametre[2];
            break;
        case '/':
            if ($tableauparametre[2] == 0) {
                echo '0';
            } else {
                echo $tableauparametre[0] / $tableauparametre[2];
            }
            break;
        case '%':
            echo $tableauparametre[0] % $tableauparametre[2];
            break;
        default:
            echo $errormsg;
    }
    echo "\n";
// Saut de ligne qui s'applique à toutes les possibilités de messages
} else {
    echo $errormsg;
    exit();
    // Sinon on affiche le message d'erreur et on quitte le programme
}
// <?php
// autre méthode
// const INVALID_PARAMETERS = 'Incorrect Parameters' . "\n";
// // Verification qu'il y a exactement 3 arguments
// if ($argc !== 4) {
//     echo INVALID_PARAMETERS;
//     exit();
// }

// // Verification que les arguments ne sont pas null
// if (isset($argv[1], $argv[2], $argv[3])) {
//     // Verification que les arguments 1 et 3 sont bien des nombres
//     if (!is_numeric($argv[1]) || !is_numeric($argv[3])) {
//         echo INVALID_PARAMETERS;
//         exit();
//     }
//     // création d'une variable pour chaque argument + supression des espaces
//     $var1 = trim($argv[1]);
//     $operation = trim($argv[2]);
//     $var2 = trim($argv[3]);

//     // Opération mathématique en fonction de l'opérateur $operation
//     switch ($operation) {
//         case '+':
//             echo $var1 + $var2 . "\n";
//             break;

//         case '-':
//             echo $var1 - $var2 . "\n";
//             break;

//         case '*':
//             echo $var1 * $var2 . "\n";
//             break;

//         case '/':
//             // if ($var2 == 0) {
//             //     echo '0' . "\n";
//             // } else {
//             //     echo $var1 / $var2 . "\n";
//             // }
//             echo($var2 == 0 ? '0' : $var1 / $var2) . "\n";
//             break;

//         case '%':
//             echo $var1 % $var2 . "\n";
//             break;

//         default:
//             echo INVALID_PARAMETERS;
//             break;
//     }
// } else {
//     echo INVALID_PARAMETERS;
//     exit();
// }
