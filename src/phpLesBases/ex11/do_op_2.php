<?php

// Récupérer un seul et unique paramètre
// Nombre | Opérateur | Nombre
// Afficher un message d'erreur dans un cas échéan
// Supprimer la présence d'espace dans les paramètres

const REGEXOSAURUS = "/^\s*(?<num1>[\+-]?\d*\.?\d+)\s*(?<operator>\+|\*|\/|\-|\%)\s*(?<num2>(?1))\s*$/";
const ERROR_SYNTAX = "Syntax Error\n";
const ERROR_PARAM = "Incorrect Parameters\n";
// On définie nos 3 constantes (Le regex, les deux messages d'erreurs)

if (!isset($argv[1]) || (isset($argv[2]))) {
    echo ERROR_PARAM;
    exit();
}
// Si le paramètre 1 est vide ou si il y en a plus d'un alors on affiche le message d'erreur et on quitte

if (!preg_match_all(REGEXOSAURUS, $argv[1], $tab, PREG_SET_ORDER)) {
    echo ERROR_SYNTAX;
    exit();
}
// On applique la regex au preg_match qui renvoie true ou false, dans le cas ou c'est false :
// On affiche l'erreur et on quitte
// Dans le cas ou c'est true : On récupère le tableau dans $tab

switch ($tab[0]['operator']) {
    // On fait un switch case avec comme paramètre l'opérateur stocké dans le tableau
    case '+':
        echo $tab[0]['num1'] + $tab[0]['num2'] . "\n";
        break;
    case '-':
        echo $tab[0]['num1'] - $tab[0]['num2'] . "\n";
        break;
    case '*':
        echo $tab[0]['num1'] * $tab[0]['num2'] . "\n";
        break;
    case '/':
        if ($tab[0]['num2'] == 0) {
            echo "0\n";
        } else {
            echo $tab[0]['num1'] / $tab[0]['num2'] . "\n";
        }
        break;
    case '%':
        echo abs(fmod($tab[0]['num1'], $tab[0]['num2'])) . "\n";
        // Cette ligne permet de faire des modulo de nombres décimaux
        break;
    default:
        echo ERROR_SYNTAX;
        // Si aucun des opérateurs plus hauts sont trouvés, on affiche une erreur
}
// <?php
// autre méthode
// if ($argc != 2) {
//     echo 'Incorrect Parameters';
//     echo "\n";
//     exit;
// }

// $my_arg = $argv[1];

// // remove whitespace
// $my_arg_trim = trim($my_arg);
// // string split
// $my_arg_split = str_split($my_arg_trim);
// // get the first char
// $first_c = $my_arg_split[0];
// if (is_numeric($first_c)) {
//     // save tab before implode
//     $tab_op = $my_arg_split;
//     // translate into string
//     $tab_to_string = implode($my_arg_split);
//     $flag = false;
//     // iterate previous tab
//     foreach ($tab_op as $op) {
//         // [\*|\+|\-|\/|\%]
//         // look for operator & save (flag => only once!!)
//         if (preg_match('/\+|\*|\/|\-|\%/', $op) && $flag == false) {
//             $flag = true;
//             $op1 = $op;
//         } elseif (preg_match('/\+|\*|\/|\-|\%/', $op) && $flag == true) {
//             // two operators
//             echo 'Syntax Error';
//             echo "\n";
//             exit;
//         }
//     }
//     // separation string into array
//     $new_tab = preg_split('/\+|\*|\/|\-|\%/', $tab_to_string);
//     $n1 = $new_tab[0];
//     $n2 = $new_tab[1];
//     // do the ops
//     if ($op1 == '+') {
//         echo $n1 + $n2;
//         echo "\n";
//     } elseif ($op1 == '-') {
//         echo $n1 - $n2;
//         echo "\n";
//     } elseif ($op1 == '*') {
//         echo $n1 * $n2;
//         echo "\n";
//     } elseif ($op1 == '/') {
//         if ($n2 != 0) {
//             echo $n1 / $n2;
//             echo "\n";
//         } else {
//             echo 0;
//             echo "\n";
//         }
//     } elseif ($op1 == '%') {
//         echo $n1 % $n2;
//         echo "\n";
//     }
// } else {
//     echo 'Syntax Error';
//     echo "\n";
//     exit;
// }
