<?php

// Récupérer l'ensemble des paramètres (argv?)
// Les afficher ET trier par ordre majuscule > minuscule
// Les trier AUSSI par ordre alphabétique

$tableau = array_slice($argv, 1);
$chaine = implode(' ', $tableau);
// On récupère le contenu du argv qu'on stock dans une
// variable sous forme de string grâce au implode

$words = explode(' ', $chaine);
sort($words);
// On remet sous la forme de tableau afin de trier

foreach ($words as $word) {
    echo $word . "\n";
}
// On affiche le tableau
// <?php
// autre méthode
// $list = [];

// function ft_split($input)
// {
//     $tableau = preg_split("/[\s]+/", $input, -1, PREG_SPLIT_NO_EMPTY);

//     sort($tableau);

//     return $tableau;
// }

// $tableau = array_slice($argv, 1);

// sort($tableau);

// for ($i = 1; $i < $argc; ++$i) {
//     foreach (ft_split($argv[$i]) as $mot) {
//         array_push($list, $mot);
//     }
// }

//     sort($list, SORT_STRING);

// foreach ($list as $value) {
//     print_r($value);
//     echo "\n";
// }
