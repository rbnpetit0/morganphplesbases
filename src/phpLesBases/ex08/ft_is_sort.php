<?php

// Créer une fonction
// Elle retourne un booléen (vrai ou faux)
// Selon si le tableau est trié ou pas
// On appelle cette fonction via main.php

function ft_is_sort($tab)
{
    // Fonction qui appelle $tab comme paramètre

    $tabsorted = $tab;
    sort($tabsorted);
    // On crée une copie de tab et on la trie

    if ($tabsorted == $tab) {
        $ft_is_sort = true;
    } else {
        $ft_is_sort = false;
    }
    // Si la copie est égale à l'original après avoir été triée
    // Alors l'original est trié, dans ce cas on return true
    // Sinon on return false

    return $ft_is_sort;
}
// <?php
// autre méthode
// function ft_is_sort($tab)
// {
//     $temp = $tab;
//     sort($temp);
//     if ($temp == $tab) {
//         return true;
//     } else {
//         return false;
//     }
// }
