<?php

$tableau_numerique = [];
$tableau_lettres = [];
$tableauspeciaux = [];
// Nous déclarons pour plus tard 3 tableaux associés à chaque type de caractère.

$argv1 = array_slice($argv, 1);
// On récupère les valeurs entrées dans le premier paramètre dans l'array $argv1
$mots = implode(' ', $argv1);
// On explose le tableau en string afin de séparer les cas de plusieurs
// mots contenus en une seule valeur (ex: "1948372 AhAhAh")
$tableau = preg_split("/\s/", $mots, -1, PREG_SPLIT_NO_EMPTY);
// On utilise preg_split afin de tout remettre sous la forme de tableau

foreach ($tableau as $value) {
    if (ctype_alpha($value)) {
        $tableau_lettres[] = $value;
    }
    // On rentre tous les mots dans un tableau associé aux mots uniquement grâce à ctype_alpha
    elseif (ctype_digit($value)) {
        $tableau_numerique[] = $value;
    }
    // On fait de même pour les nombres grâce à ctype_digit
    else {
        $tableauspeciaux[] = $value;
    }
    // Pour finir tout ce qui n'est pas un mot ou un nombre rentre dans la dernière catégorie
}
sort($tableau_numerique, SORT_STRING);
natcasesort($tableau_lettres);
sort($tableauspeciaux);
// On trie les trois tableaux de la façon que l'on souhaite.

$tableauclasse = array_merge($tableau_lettres, $tableau_numerique, $tableauspeciaux);
// On fusionne les trois tableaux en un à l'aide d'array_merge
$resultatfinal = implode("\n", $tableauclasse) . "\n";
// On le fait passer en string;
echo $resultatfinal;
// On l'affiche
// <?php
// autre méthode
// // Création d'une liste vide
// $list = [];

// // Récupérer la fonction de l'ex03 qui prend une chaine de caractères en
// // argument, et renvoie un tableau trié des différents mots)

// function ft_split($input)
// {
//     if (preg_split("/[\s]+/", $input, -1, PREG_SPLIT_NO_EMPTY) != false) {
//         $tableau = preg_split("/[\s]+/", $input, -1, PREG_SPLIT_NO_EMPTY);

//         return $tableau;
//     }
// }

// // récupere le tableau
// $tableau = array_slice($argv, 1);
// // Fonction de tri

// // je crée une boucle avec le nombre d'élèments
// for ($i = 1; $i < $argc; ++$i) {
//     foreach (ft_split($argv[$i]) as $mot) {
//         // je créé un tableau
//         array_push($list, $mot);
//     }
// }

//  // je trie tous les mots de mon tableau dans un ordre logique

// $tableau_numeric = [];
// $tableau_lettres_a_z = [];
// $tableau_trash = [];
// // j'appelle chaque valeur dans mon tableau et je la renvoie
// foreach ($list as $value) {
//     // if (preg_match('/\b[[:alpha:]]/', $value) == 1) {
//     if (ctype_alpha($value) === true) {
//         $tableau_lettres_a_z[] = $value;
//     } elseif (is_numeric($value) === true) {
//         $tableau_numeric[] = $value;
//     } else {
//         $tableau_trash[] = $value;
//     }
// }
// natcasesort($tableau_lettres_a_z);

// foreach ($tableau_lettres_a_z as $letters) {
//     echo "$letters\n";
// }
// sort($tableau_numeric, SORT_STRING);
// foreach ($tableau_numeric as $chiffre) {
//     echo "$chiffre\n";
// }
// sort($tableau_trash);
// foreach ($tableau_trash as $trash) {
//     echo "$trash\n";
// }
// // boucle avec affichage standart des mots
