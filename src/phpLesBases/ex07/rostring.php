<?php

// Récupérer uniquement le PREMIER paramètre
// Gérer le cas où celui-ci est vide
// Mettre le premier mot de ce paramètre en DERNIERE place
// Réafficher le tout séparer d'un espace

if (!isset($argv[1])) {
    exit();
}
$abc = $argv[1];
// Récupérer le paramètre 1 du terminal
$tableau = explode(' ', $abc);
// Transformer le string en array (tableau)

array_push($tableau, $tableau[0]);
array_shift($tableau);
// Effectuer l'organisation du tableau comme demandé

$bca = implode(' ', $tableau);
$stringfinal = preg_replace('/\s+/', ' ', $bca);
// Remettre le tableau sous forme de string et supprimer
// les espaces invisibles

echo $stringfinal . "\n";
// Afficher le résultat
// <?php
// autre méthode
// // je vérifie que le nombre d'éléments est supérieur à 1
// if ($argc > 1) {
//     // je récupère l'élément du tableau qui m'intéresse
//     array_slice($argv, 1);
//     $index = $argc;

//     // je donne une valeur a la chaine de commande qui m'intéresse
//     $chaine = $argv[1];
//     // je split ma chaine de commande pour la rendre lisible
//     $tab = preg_split("/[^\S\r\n]/", $chaine, -1, PREG_SPLIT_NO_EMPTY);
//     // je donne une valeur au premier mot de la chaine
//     $debut = $tab[0];
//     // j'extraie la portion du tableau qui m'intéresse
//     $tab = array_slice($tab, 1);
//     // je créé un tableau en rajoutant mon mot à la fin de mon tableau
//     array_push($tab, $debut);
//     // je change mon tableau en string avec le mot du début déplacé à la fin
//     echo implode(' ', $tab) . "\n";
// }
