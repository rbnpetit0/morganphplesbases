<?php

$tab = array_slice($argv, 1);
// On récupère les valeurs du tableau en skippant celle du code
$toto = $tab[0];
// On stock la première valeur du tableau dans une variable toto
array_shift($tab);
// On supprime la première valeur du tableau qui est maintenant
// stockée dans toto

foreach ($tab as $paire) {
    $chars = preg_split('/\:/', $paire, -1, PREG_SPLIT_OFFSET_CAPTURE);
}
// On fait une boucle qui enregistre les variables dans un tableau $chars

if (!isset($chars[0][0]) || !isset($chars[1][0]) || empty($chars[1][0])) {
    exit();
}
// Si les variables sont vides alors on exit

if ($toto == $chars[0][0]) {
    echo $chars[1][0] . "\n";
}
// On regarde si la variable de $chars correspond bien à la toute première
// enregistrée dans $toto, si oui alors on affiche la valeur correspondante
// <?php
// autre méthode
// if ($argc > 2) {
//     $tab = [];
//     $word = $argv[1];
//     $array = array_splice($argv, 2);

//     foreach ($array as $value) {
//         $newValue = preg_split('/\W|s+/', $value, -1, PREG_SPLIT_NO_EMPTY);
//         @$tab[$newValue[0]] = $newValue[1];
//     }
//     if (count($newValue, 0) == 2) {
//         echo "$tab[$word]\n";
//     }
// }
